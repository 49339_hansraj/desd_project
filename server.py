# import the flask package
from flask import Flask, request, render_template
import database
import mysql.connector

# create a server instance
app = Flask(__name__)

@app.route("/update", methods=["POST"])
def post_sensor_data():
  temperature = request.args.get("temperature");
  humidity = request.args.get("humidity");
  smoke = request.args.get("smoke");
  print(f"Temperature : {temperature}°C");
  print(f"humidity in '%' of Relative Humidity : {humidity}'%' RH");
  print(f"Smoke : {smoke}PPM");
  database.insert_sensordata(temperature=temperature, humidity=humidity, smoke=smoke)
  database.thingspeak_post(temperature=temperature, humidity=humidity, smoke=smoke)
  return "value is recorded";
 
@app.route("/result")
def result():
  connection =  mysql.connector.connect(
      host="localhost", user='root', database='iot_project', password="Hans@007")
  # get cursor
  cursor = connection.cursor()
  query = """SELECT Temperature FROM data ORDER BY id DESC LIMIT 1"""
  query1 = """SELECT Humidity FROM data ORDER BY id DESC LIMIT 1"""
  query2 = """SELECT Smoke FROM data ORDER BY id DESC LIMIT 1"""
  cursor.execute(query)
  temperature = cursor.fetchone()[0];
  cursor.execute(query1)
  humidity = cursor.fetchone()[0];
  cursor.execute(query2)
  smoke = cursor.fetchone()[0];
  cursor.close()
  return render_template("result.html",T_value=temperature, H_value=humidity, S_value=smoke)


# start the server
# host: 0.0.0.0 for accepting network connections
# port: used to run the server
# debug: used while debugging the application
app.run(host="0.0.0.0", port=4000, debug=True)