#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "DHT.h"

/* 
- wifi name (ssid) and password  
- used to connect to WiFi
*/
const char *ssid = "hans";
const char *password = "Hans0007";

#define DHTTYPE DHT11
uint8_t DHTPin = D4;
DHT dht(DHTPin, DHTTYPE);

void send_temperature_to_server(float temperature, float humidity, float smoke) {
  /* url to send the temperature to the flask server */
   String data= "temperature=" + String(temperature) + "&humidity=" + String(humidity) + "&smoke=" + String(smoke);
  String url1 = "http://    192.168.148.37:4000/update?" + String(data);
  /* client for connecting to the server */
  WiFiClient client;
  HTTPClient http;

  Serial.println("Sending a POST request");

  /* start sending the request */
  http.begin(client, url1);

  /* send a POST request */
  int status_code = http.POST("");
  Serial.println("Status Code: " + String(status_code));
 
  if (status_code > 0) {
    if (status_code == HTTP_CODE_OK) {
      Serial.println("successfully sent the request");
    } else {
      Serial.println("Error while processing the request");
    }
  } else {
    Serial.println("Error while sending request");
  }
  /* end sending the request */
  http.end();
}

void setup() {
  Serial.begin(115200);
  Serial.flush();

  /* A0 input*/
  pinMode(A0, INPUT);
  /*D4(DHTPin) input*/
  pinMode(DHTPin, INPUT);
  dht.begin();
  /* WiFi station */
  WiFi.mode(WIFI_STA);

  /* connect to the WiFi */
  Serial.println("Connecting to WiFi");
  WiFi.begin(ssid, password);

  /* wait for the successfull connection */
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.print("Connected to the WiFi: ");

  /* print NodeMCU IP address */
  Serial.println(WiFi.localIP());
  
}

void loop() {

  /* read the current sensor values */
  float smoke = analogRead(A0);
  Serial.println("Smoke : " + String(smoke));
  float temperature = dht.readTemperature();
  Serial.println("Temperature : " + String(temperature));
  float humidity = dht.readHumidity();
  Serial.println("Humidity : " + String(humidity));
  send_temperature_to_server(temperature, humidity, smoke);
  delay(1000);  
}
